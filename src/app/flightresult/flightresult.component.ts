import { google } from '@agm/core/services/google-maps-types';
import { Component, OnInit } from '@angular/core';
import { FlugsucheServiceService } from 'app/service/flightRequests';

@Component({
  selector: 'app-flightresult',
  templateUrl: './flightresult.component.html',
  styleUrls: ['./flightresult.component.css']
})
export class FlightresultComponent implements OnInit {

  lat: number = 0.0000;
  lng:number= 0.000;

  data:any;
  countryData:any;

  countryCode= '';
  mapIDFrom = '';
  mapIDTo = '';
  cityName = '';

  hasReturn = false;

  countryScore: any;
  advice='';
  advicelink='';
  

  constructor(private flugsucheService: FlugsucheServiceService) { }

  ngOnInit(): void {
    this.data = JSON.parse(localStorage.getItem('flughtresult:'));
   
    this.getMapID();
    this.getCountryCode();
    this.getCityName()
    this.getLatLng(); 
    
    this.flugsucheService.getCountryInformation(this.countryCode).subscribe(
      (data: any) => {
        console.log('get data...', this.countryCode)
        this.countryData = data.data[this.countryCode].advisory;
        console.log(this.countryData)
        this.countryScore = this.countryData.score;
        this.advice = this.countryData.message;
        this.advicelink = this.countryData.source;
      }
    );
   
  }

  checkForReturn(){
    if(this.data.route.length>1){
     return true;
    } else {
      return false;
    }
  }


  openLink(){
    window.open(this.advicelink, "_blank");
  }

  openBooking(){
    window.open(this.data.deep_link, "_blank");
  }


  getCityName() {
   
      this.cityName =  this.data.route[0].cityTo;
    
  }

  getMapID(){
  
    this.mapIDTo = this.data.route[0].cityTo.toLowerCase() + '_' +  this.data.countryTo.code.toLowerCase();
    console.log(this.mapIDTo)
    
    this.mapIDFrom = this.data.data[0].cityFrom.toLowerCase() + '_' +  this.data.countryFrom.code.toLowerCase();
   
  }

  getCountryCode(){
    this.countryCode =  this.data.countryTo.code.toLowerCase();
  }

  getLatLng(){
 
      
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({
      'address': this.cityName
    }, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        this.lat = results[0].geometry.location.lat();
        this.lng = results[0].geometry.location.lng();
        var myOptions = {
          zoom: 11,
          center: new google.maps.LatLng(this.lat, this.lng)
        };
        
      } else {
        alert("Something got wrong " + status);
      }
    });
    
  }

  getCountryData() {
    
    console.log('get data...', this.countryCode)
   
    
  }

 
  timestampToDate(timestamp: number) {
    var date = new Date(timestamp * 1000);

    var year = date.getFullYear();

    var month = date.getMonth() + 1;

    var day = date.getDate();
    // Hours part from the timestamp
    var hours = date.getHours();
    // Minutes part from the timestamp
    var minutes = "0" + date.getMinutes();

    return day + '.' + month  + '.' + year
  }

  timestampToTime(timestamp: number) {
    var date = new Date(timestamp * 1000);


    // Hours part from the timestamp
    var hours = date.getHours();
    // Minutes part from the timestamp
    var minutes = "0" + date.getMinutes();
    return hours + ':' + minutes.substr(-2);
  }


}
