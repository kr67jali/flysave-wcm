import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class  FlugsucheServiceService {
  message: string

  apiKey = 'EPuvrYAJ9zQ_QIPkNp-P0d80ZukvzXgB';

  headerDict = {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    apiKey: this.apiKey
  }
  
  requestOptions = { headers: new HttpHeaders().append('apiKey', 'EPuvrYAJ9zQ_QIPkNp-P0d80ZukvzXgB')}



  constructor(private http: HttpClient) { }

  getHinFlug(from: String, to: String, dateFrom:String, adults: number, children: number) {
    let http = 'https://tequila-api.kiwi.com/v2/search?apiKey=EPuvrYAJ9zQ_QIPkNp-P0d80ZukvzXgB&flyFrom='+ from +'&to='+ to +'&dateFrom=' + dateFrom + '&dateTo='+ dateFrom + '&direct_flights=1&adults='+adults+'&children='+children+'&partner=picky&v=3';
    return this.http.get(http, this.requestOptions);
  }

  getHinUndRueckFlug(from: String, to: String, dateFrom:String, dateTo:String, adults: number, children: number) {
    return this.http.get('https://tequila-api.kiwi.com/v2/search?flyFrom='+ from +'&to='+ to +'&dateFrom=' + dateFrom + '&dateTo='+ dateFrom + '&return_from='+dateTo+'&return_to='+dateTo+'&direct_flights=1&adults='+adults+'&children='+children+'&partner=picky&v=3', this.requestOptions);
  }

  getCountryInformation(countryCode: String) {
    return this.http.get('https://www.travel-advisory.info/api?countrycode='+ countryCode);
  }

}
