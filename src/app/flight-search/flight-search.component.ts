import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FlugsucheServiceService } from 'app/service/flightRequests';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-flight-search',
  templateUrl: './flight-search.component.html',
  styleUrls: ['./flight-search.component.css']
})
export class FlightSearchComponent implements OnInit {

  adults: number;
  children: number;
  numberOfPass = 1;
  enoughPass = true;

  hinflugDateInput: any;
  rueckflugDateInput: any;
  hinflugDate = "";
  rueckflugDate = "";

  fromAirport = '';
  toAirport = '';

  fromInputValidation = true;
  toInputValidation = true;

  fromDateValidation = true;
  toDateValidation = true;

  searchresults = [];

  airports = []
  filedbList: string[];
  flightMode: String;
  closeResult: string;
  selectedFromAirport = [];
  selectedToAirport = [];
  dropdownSettings1 = {};
  dropdownSettings2 = {};
  isTitle: boolean;
  dropdownList = [];
  min = new Date();
  max = new Date(this.min.getFullYear(), this.min.getMonth() + 6, this.min.getDate());

  constructor(private modalService: NgbModal,
    private httpClient: HttpClient,
    private flugsucheService: FlugsucheServiceService,
    private router: Router) { }



  public data: any;

  ngOnInit(): void {
    this.children = 0;
    this.adults = 1;
    this.retrieveData()

    this.flightMode = "Hin- und Rückflug"
    this.dropdownList = this.airports


    this.dropdownSettings1 = {
      singleSelection: true,
      text: "Abflug",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "",
      lazyLoading: true
    };

    this.dropdownSettings2 = {
      singleSelection: true,
      text: "Ankunft",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "",
      lazyLoading: true
    };

  }

  buildHinDate() {
    if (this.hinflugDateInput) {
      if (typeof this.hinflugDateInput === 'string') {
        const newDate = new Date(this.hinflugDateInput);

        this.hinflugDate = newDate.getDate() + '.' + newDate.getMonth() + 1 + '.' + newDate.getFullYear();
        this.fromDateValidation = true;
      } else {
        this.hinflugDate = this.hinflugDateInput.day + '/' + this.hinflugDateInput.month + '/' + this.hinflugDateInput.year;
        this.fromDateValidation = true;

      }

    } else {
      this.fromDateValidation = false;
    }

  }

  buildRueckDate() {
    if (this.rueckflugDateInput) {
      if (typeof this.rueckflugDateInput === 'string') {
        const newDate = new Date(this.rueckflugDateInput);

        this.rueckflugDate = newDate.getDate() + '.' + newDate.getMonth() + 1 + '.' + newDate.getFullYear();
        this.toDateValidation = true;
      } else {
        this.rueckflugDate = this.rueckflugDateInput.day + '/' + this.rueckflugDateInput.month + '/' + this.rueckflugDateInput.year;
        this.toDateValidation = true;
      }
    } else {
      this.toDateValidation = false;
    }

  }

  getIATA() {
    if (this.selectedFromAirport[0]) {
      this.fromAirport = this.selectedFromAirport[0].iata;
      this.fromInputValidation = true;
    }
    if (this.selectedToAirport[0]) {
      this.toAirport = this.selectedToAirport[0].iata;
      this.toInputValidation = true;
    }
  }

  getData() {
    console.log('get data...')
    if (this.flightMode === 'Hin- und Rückflug') {
      this.flugsucheService.getHinUndRueckFlug(this.fromAirport, this.toAirport, this.hinflugDate, this.rueckflugDate, this.adults, this.children).subscribe(
        (data: any) => {

          this.searchresults = data.data
          console.log(data)
          localStorage.setItem('flightsearch:', JSON.stringify(data))
        }
      );
    }
    if (this.flightMode === 'Nur Hinflug') {
      this.flugsucheService.getHinFlug(this.fromAirport, this.toAirport, this.hinflugDate, this.adults, this.children).subscribe(
        (data: any) => {
          this.searchresults = data.data
          console.log(data)
          localStorage.setItem('flightsearch:', JSON.stringify(data))
        }
      );
    }

  }

  inputValidation() {
    if (this.flightMode === 'Hin- und Rückflug') {
      this.buildHinDate();
      this.buildRueckDate();
    } else {
      this.buildHinDate();
    }

    this.getIATA();
    if (!this.fromAirport) {
      this.fromInputValidation = false;
    }
    if (!this.toAirport) {
      this.toInputValidation = false;
    }
    if (!this.hinflugDate) {
      this.fromDateValidation = false;
    }
    if (!this.rueckflugDate) {
      this.toDateValidation = false;
    }

    if (this.fromAirport && this.toAirport && this.hinflugDate && this.rueckflugDate && this.flightMode === 'Hin- und Rückflug') {
      this.getData();
    } else if (this.fromAirport && this.toAirport && this.hinflugDate && this.flightMode === 'Nur Hinflug') {
      this.getData();
    }
  }


  retrieveData() {
    this.getJSONFromLocal().subscribe(
      data =>
        //Bind the data emitted by JSON file to model class here
        data.map(airports =>
          this.dropdownList = this.dropdownList.concat({ 'itemName': airports.city + ' (' + airports.iata_code + ')', 'iata': airports.iata_code })
        )
      ,
      error => console.error(`Failed because: ${error}`));
  }

  increasePassenger(adult: boolean) {
    if (adult) {
      this.adults++;
    } else {
      this.children++;
    }

    this.enoughPass = true;
    this.getNumberOfPass();
  }

  decreasePassenger(adult: boolean) {

    if (adult) {
      if (this.adults > 0) {
        this.adults--;
      }
    } else {
      if (this.children > 0) {
        this.children--;
      }
    }
    if (this.adults == 0 && this.children == 0) {
      this.enoughPass = false;
    } else {
      this.enoughPass = true;
    }
    this.getNumberOfPass();
  }

  getNumberOfPass() {
    this.numberOfPass = this.adults + this.children;
  }

  timestampToDate(timestamp: string) {
    const unixTimeZero = Date.parse(timestamp);
    var date = new Date(timestamp);

    var year = date.getFullYear();

    var month = date.getMonth() + 1;

    var day = date.getDate();
    // Hours part from the timestamp
    var hours = date.getHours();
    // Minutes part from the timestamp
    var minutes = "0" + date.getMinutes();

    return day + '.' + month  + '.' + year
  }

  timestampToTime(timestamp: string) {
    const unixTimeZero = Date.parse(timestamp);

    var date = new Date(timestamp);


    // Hours part from the timestamp
    var hours = date.getHours();
    // Minutes part from the timestamp
    var minutes = "0" + date.getMinutes();
    return hours + ':' + minutes.substr(-2);
  }



  switchMode(mode: String) {
    if (mode === "Hin- und Rückflug") {
      this.flightMode = "Hin- und Rückflug";
    } else if (mode === "Nur Hinflug") {
      this.flightMode = "Nur Hinflug";
    }
  }

  open(content, type) {
    if (type === 'sm') {
      console.log('aici');
      this.modalService.open(content, { centered: true, size: 'sm' }).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
    } else {
      this.modalService.open(content, { centered: true }).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
    }
  }

  onItemSelect(item: any) {
    if (item.id === 1) {
      this.isTitle = true;
    } else if (item.id === 2) {
      this.isTitle = false;
    }
  }

  OnItemDeSelect(item: any) {
    console.log(item);
  }

  onSelectAll(items: any) {
    console.log(items);
  }

  onDeSelectAll(items: any) {
    console.log(items);
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  public getJSONFromLocal(): Observable<any> {
    return this.httpClient.get("assets/airport.json");
  }



  getcities(dropdownList: any) {
    dropdownList =
      [
        { 'itemName': "Atlanta" },
        { 'itemName': "Chicago" },
        { 'itemName': "Beijing" },
        { 'itemName': "London" },
        { 'itemName': "Paris" }

      ]

    return dropdownList;
  }

  selectAndRedirect(data:any) {
    localStorage.setItem('flughtresult:', JSON.stringify(data))
    this.router.navigate(['/app/flightresult']);
  }

}