import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { FlightSearchComponent } from './flight-search/flight-search.component';
import { FlightresultComponent } from './flightresult/flightresult.component';

const routes: Routes =
[
    { path: 'app/flightsearch',     component: FlightSearchComponent },
    { path: 'app/flightresult',     component: FlightresultComponent },
    { path: '', redirectTo: 'app/flightsearch', pathMatch: 'full' }
];

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        RouterModule.forRoot(routes,{
          useHash: true
        })
    ],
    exports: [
    ],
})
export class AppRoutingModule { }
