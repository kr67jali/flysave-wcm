import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; // this is needed!
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, NgbPopover, NgbTooltip } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app.routing';

import { AppComponent } from './app.component';

import { LandingPageComponent } from './landing-page/landing-page.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HttpClientJsonpModule, HttpClientModule } from '@angular/common/http';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { FlightSearchComponent } from './flight-search/flight-search.component';
import { FlightresultComponent } from './flightresult/flightresult.component';

import { AgmCoreModule } from '@agm/core';


@NgModule({
    declarations: [
        AppComponent,
        LandingPageComponent,
        NavbarComponent,
        FlightSearchComponent,
        FlightresultComponent,
    ],
    imports: [
        BrowserAnimationsModule,
        NgbModule,
        FormsModule,
        RouterModule,
        AppRoutingModule,
        HttpClientModule,
        AngularMultiSelectModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyCySqngjpD6etzw0mawGNzx3q-RWKfKeS0'
          }),
          HttpClientJsonpModule,
          AppRoutingModule,

    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
0
