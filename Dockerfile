FROM node:12.2.0

RUN mkdir /app
WORKDIR /app

ENV PATH /app/node_modules/.bin:$PATH

COPY package.json /app/package.json
RUN npm install
RUN npm install -g @angular/cli@7.3.9

COPY . /app

CMD ["sh", "-c", "ng serve --host 0.0.0.0 --port 4200"]
